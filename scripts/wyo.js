$(document).ready( function(){

  //Animate breadcrumb trail  
  if ( $("#breadcrumb_wrapper").length > 0 ) {

    //hide the breadcrumb here so that non-js browsers will still see it
    $('.breadcrumb_wrapper').css("display","none");

    //display the breadcrumb open button
    $('#breadcrumb_open').slideDown("slow");

    $("#breadcrumb_open").bind("click",function(){
      $('#breadcrumb_open').hide();
      $('#breadcrumb_close').show();
      $('.breadcrumb_wrapper').slideDown("slow");
    });

    $('#breadcrumb_close').bind("click",function(){
      $('#breadcrumb_close').hide();
      $('#breadcrumb_open').show();
      $('.breadcrumb_wrapper').slideUp("slow");
    });
  }

  //Round Primary Links
  settingsPrimary = {
    tl: { radius: 0 },
    tr: { radius: 0 },
    bl: { radius: 5 },
    br: { radius: 5 },
    antiAlias: true,
    autoPad: false
  }
  var cornersObjPrimary = new curvyCorners(settingsPrimary, "plink_div");
  cornersObjPrimary.applyCornersToAll();

  //Round Secondary Links
  settingsSecondary = {
    tl: { radius: 1 },
    tr: { radius: 5 },
    bl: { radius: 1 },
    br: { radius: 5 },
    antiAlias: true,
    autoPad: false
  }
  var cornersObjSecondary = new curvyCorners(settingsSecondary, "slink_div");
  cornersObjSecondary.applyCornersToAll();

  //Round Left Blocks
  settingsLeftBlock = {
    tl: { radius: 1 },
    tr: { radius: 10 },
    bl: { radius: 1 },
    br: { radius: 10 },
    antiAlias: true,
    autoPad: false
  }
  $('#sidebar-left-inner').children('.block').each( function() {
    var cornersObjLeftBlock = new curvyCorners(settingsLeftBlock, this);
    cornersObjLeftBlock.applyCornersToAll();
  });

  //Round Left Blocks Titles
  settingsLeftBlockTitle = {
    tl: { radius: 1 },
    tr: { radius: 10 },
    bl: { radius: 1 },
    br: { radius: 1 },
    antiAlias: true,
    autoPad: false
  }
  $('#sidebar-left-inner .block_title').each( function() {
    var cornersObjLeftBlockTitle = new curvyCorners(settingsLeftBlockTitle, this  );
    cornersObjLeftBlockTitle.applyCornersToAll();
  });

  //Round Right Blocks
  settingsRightBlock = {
    tl: { radius: 10 },
    tr: { radius: 1 },
    bl: { radius: 10 },
    br: { radius: 1 },
    antiAlias: true,
    autoPad: false
  }
  $('#sidebar-right-inner').children('.block').each( function() {
    var cornersObjRightBlock = new curvyCorners(settingsRightBlock, this);
    cornersObjRightBlock.applyCornersToAll();
  });

  //Round Right Blocks Titles
  settingsRightBlockTitle = {
    tl: { radius: 10 },
    tr: { radius: 1 },
    bl: { radius: 1 },
    br: { radius: 1 },
    antiAlias: true,
    autoPad: false
  }
  $('#sidebar-right-inner .block_title').each( function() {
    var cornersObjRightBlockTitle = new curvyCorners(settingsRightBlockTitle, this);
    cornersObjRightBlockTitle.applyCornersToAll();
  });
});
